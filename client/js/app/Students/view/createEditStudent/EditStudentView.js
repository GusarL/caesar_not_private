'use strict';

(function (This, app) {
    This.EditStudentView = Backbone.View.extend({
        tag: 'section',

        className: 'backdrop',

        template: templates.editStudentViewTpl,

        events: {
            'drop .dropZone': 'handleDroppedFiles',
            'dragover .dropZone': 'handleDragOver',
            'click .close-modal-window': 'exit',
            'click .save-changes': 'editStudent',
            'click .BrowsePhoto': 'openPhotoSelector',
            'click .BrowseCV': 'openCVSelector',
            'click .remove-photo': 'removePhoto',
            'click .remove-cv': 'removeCV',
            'change .approvedBy': 'showCustom',
            'change [name="uploadPhoto"]': 'handleSelectedPhotos',
            'change [name="uploadCV"]': 'handleSelectedCVs'
        },

        initialize: function (options) {
            this.mediator = app.mediator;
            this.listener = {
                'students': {view: 'StudentsListView'},
                'editStudent': {view: 'EditStudentListView'}
            };

            this.group = options.group;
            this.attachment = options.attachment;

            this.fileStore = {
                cv: [],
                photos: [],
                toDelete: []
            };

            this.attachmentStore = {
                key: '',
                cv: [],
                photo: []
            };
        },

        render: function () {
            var groupExperts = this.group.get('experts'),
                groupName = this.group.get('name'),
                model = this.model.toJSON();

            if (this.attachment) {
                model.cv = this.attachment.get('cv');
                model.photo = this.attachment.get('photo');
            } else {
                model.cv = [];
                model.photo = [];
            }

            model.groupName = groupName;
            model.experts = groupExperts;

                
            this.$el.html(this.template(model));

            return this;
        },

        editStudent: function () { 
            var incomingScore = this.$el.find('[name=incomingTest]').val(),
                studentSurname = this.$el.find('[name=LastName]').val(),
                studentName = this.$el.find('[name=FirstName]').val(),
                englishLevel = this.$el.find('.englishLevel').val(),
                entryScore = this.$el.find('[name=entryScore]').val(),
                groupId = this.$el.find('[name=groupId]').val(),
                showHints = this.showHints.bind(this),
                context = this,
                approvedBy,
                validationDependencies,
                isPassedValid = true,
                newStudent;

                if( this.$el.find('.custom-approval-input').prop('disabled')) {
                    approvedBy = this.$el.find('.approvedBy').val();
                } else {
                    approvedBy = this.$el.find('.custom-approval-input').val();
                }

            validationDependencies = {
                studentName: [this.isName, studentName, 'You can use only letters, space and "-" ', 'FirstName'],
                studentSurname: [this.isName, studentSurname, 'You can use only letters, space and "-" ', 'LastName'],
                incomingScore: [this.isIncoming, incomingScore, 'You can use only numbers 0 - 1000', 'IncomingTest'],
                entryScore: [this.isScore.bind(this), entryScore, 'You can use only real numbers 2 - 5', 'EntryScore'],
                approvedBy: [this.isName, approvedBy,  'You can use only letters, space and "-" ', 'CustomApproval']
            };

            $.each(validationDependencies, function (key, value) {
                if(!value[0](value[1])){
                    showHints(context, value[2], value[3]);
                    isPassedValid = false;
                }
            });

            entryScore = this.$el.find('[name=entryScore]').val();

            if (isPassedValid) {
                newStudent = {
                    groupId: groupId,
                    name: studentName,
                    lastName: studentSurname,
                    englishLevel: englishLevel,
                    CvUrl: '',
                    avatar: '',
                    entryScore: entryScore,
                    incomingScore: incomingScore,
                    approvedBy: approvedBy
                };

                this.deleteFiles();

                if (this.fileStore.cv.length > 0 || this.fileStore.photos.length > 0) {
                    _.each(this.fileStore.cv, function (file) {
                        this.sendToServer(file, 'cv');
                    }, this);
                }

                if (this.fileStore.photos.length > 0) {
                    _.each(this.fileStore.photos, function (file) {
                        this.sendToServer(file, 'photo');
                    }, this);
                }

                this.saveToDataBase(studentSurname, studentName);

                this.model.set(newStudent);
                this.model.save();

                $(document).off('keydown');
                $(document).off('click');

                this.remove();

                this.mediator.publish('Students: crud-request');
            }
        },


        isName: function (value) {
            var validator = /[A-Za-z]{1}[a-z]{1,9}[ -]{0,1}[A-Za-z]{1}[a-z]{1,9}/;
            return validator.test(value);
        },

        isIncoming: function (value) {
            value = parseInt(value);
            return (value >= 0 && value <= 1000);
        },

        isScore: function (score) {
            var entry = this.$el.find('[name=entryScore]'),
                result = true,
                firstDigit, decimal;

            if (isNaN(parseFloat(score))) {
                result = false;
            }
            
            score = score.replace(',', '.');

            if (score.indexOf(".") > -1){
                firstDigit = score.slice(0, score.indexOf("."));
                decimal =  score.slice(score.indexOf(".") + 1);
            } else {
                firstDigit= parseInt(score.charAt(0));
                decimal = score.slice(1);
            }
            if (firstDigit < 2){
                score = 2.0;
            } else if (firstDigit >= 5){
                score = 5.0;
            } else {
                score = firstDigit + '.' + decimal;
                score = Math.round(parseFloat(score)*10)/10;
            }

            entry.val(score);

            return result;
        },

        exit: function () {
            $(document).off('keydown');
            $(document).off('click');
            this.remove();

            this.mediator.publish('Students: crud-request');
        },
        
        showCustom () {
            var selected = this.$el.find(".approvedBy option:selected"),
                approvalInput = this.$el.find('.custom-approval-input');

            if (selected.val() === 'Custom') {
                approvalInput.removeClass('hidden');
                approvalInput.prop('disabled', false);
            } else {
                approvalInput.addClass('hidden');
                approvalInput.prop('disabled', true);
            }
        },

        showHints: function (self, message, input) {
            var hints = [{
                    name: input,
                    text: message
                }];

            app.mediator.publish('Message', {
                type: 'hints',
                $el: self.$el,
                hints: hints
            });
        },

        openPhotoSelector: function () {
            var uploadPhoto = this.$el.find('[name="uploadPhoto"]');

            uploadPhoto.click();
        },

        openCVSelector: function () {
            var uploadCV = this.$el.find('[name="uploadCV"]');

            uploadCV.click();
        },

        handleSelectedPhotos: function (e) {
            var downloadedPhoto = this.$el.find('.downloadedPhoto'),
                browsePhoto = this.$el.find('.BrowsePhoto'),
                allowed = 'jpeg,jpg,png,tiff',
                file = e.target.files[0],
                ext;

            if(file) {
                ext = file.name.slice(file.name.lastIndexOf('.') + 1);

                if (file.size <= 5e6 && allowed.indexOf(ext) > -1) {
                    downloadedPhoto.append(templates.studentDownloadedPhotoTpl(file));

                    this.fileStore.photos.push(file);
                }
            }
        },

        handleSelectedCVs: function (e) {
            var downloadedCV = this.$el.find('.downloadedCV'),
                browseCV = this.$el.find('.BrowseCV'),
                allowed = 'doc,docx,pdf,rtf',
                file = e.target.files[0],
                ext;

            if (file) {
                ext = file.name.slice(file.name.lastIndexOf('.') + 1);

                if (file.size <= 5e6 && allowed.indexOf(ext) > -1) {
                    downloadedCV.append(templates.studentDownloadedCVTpl(file));

                    this.fileStore.cv.push(file);
                }
            }
        },

        handleDragOver: function (e) {
            e.preventDefault();
        },

        handleDroppedFiles: function (e) {
            e.preventDefault();
            e.stopPropagation();

            if (e.type === 'drop') {
                var files = e.originalEvent.dataTransfer.files,
                    downloadedPhoto = this.$el.find('.downloadedPhoto'),
                    downloadedCV = this.$el.find('.downloadedCV');

                _.each(files, function (file) {
                    var ext = file.name.slice(file.name.lastIndexOf('.') + 1);

                    if (ext === 'jpg' || ext === 'png' || ext === 'jpeg' || ext === 'tiff') {
                        downloadedPhoto.append(templates.studentDownloadedPhotoTpl(file));

                        this.store.files.photo.push(file);
                        this.store.models.photo.push({
                            name: file.name.slice(0, file.name.lastIndexOf('.')),
                            ext: file.name.slice(file.name.lastIndexOf('.') + 1),
                        });
                    } else if (ext === 'doc' || ext === 'docx' || ext === 'pdf' || ext === 'rtf') {
                        downloadedCV.append(templates.studentDownloadedCVTpl(file));
                        this.store.files.cv.push(file);
                    } else {
                        alert('Sorry, you should use only allowed file types:  doc, docx, pdf or rtf for CV and jpg, jpeg, tiff or png for photo');
                    }
                }, this);
            } 
        },

        removePhoto: function (e) {
            var li = e.currentTarget.parentNode,
                delName = e.currentTarget.dataset.photo;

            this.fileStore.toDelete.push(delName);

            li.remove();
        },

        removeCV: function (e) {
            var li = e.currentTarget.parentNode,
                delName = e.currentTarget.dataset.cv;

            this.fileStore.toDelete.push(delName);

            li.remove();
        },

        sendToServer: function (file, folder) {
            var reader = new FileReader();

            reader.readAsDataURL(file);

            reader.onloadend = function (e) {
               $.ajax({
                    url: "/supplements/" + folder + '/' + file.name,
                    data: e.target.result,
                    type: "PUT",
                    success: function (data) {
                        console.log('saved');
                    },
                    error: function (xhr, status, error) {
                        alert('File' + file.name + 'is not saved');
                    }
                });
            };
        },

        saveToDataBase: function(lastName, name) {
            var newAttachment = {
                key: lastName + ' ' + name,
                cv: [],
                photo: []
            };

            if (this.attachment) {
                newAttachment.cv = this.attachment.get('cv'),
                newAttachment.photo = this.attachment.get('photo')
            }

            _.each(this.fileStore.cv, function (item) {
                newAttachment.cv.push({
                    name: item.name.slice(0, item.name.lastIndexOf('.')),
                    ext: item.name.slice(item.name.lastIndexOf('.') + 1),
                    url: '/supplements/cv/' + item.name
                });
            });

            _.each(this.fileStore.photos, function (item) {
                newAttachment.photo.push({
                    name: item.name.slice(0, item.name.lastIndexOf('.')),
                    ext: item.name.slice(item.name.lastIndexOf('.') + 1),
                    url: '/supplements/photo/' + item.name
                });
            });

            if (this.attachment) {
                this.attachment.set(newAttachment);
                this.attachment.save();
            } else {
                store.attachments.create(newAttachment);
            }
        },

        deleteFiles: function () {
            if (this.attachment) {
                var existCVs = this.attachment.get('cv'),
                    existPhotos = this.attachment.get('photo');
            }

            _.each(this.fileStore.toDelete, function (delName) {
                this.fileStore.photos.forEach(function (item, i) {
                    if (item.name === delName) {
                        this.fileStore.photos.splice(i, 1);
                    }
                }, this);

                this.fileStore.cv.forEach(function (item, i) {
                    if (item.name === delName) {
                        this.fileStore.cv.splice(i, 1);
                    }
                }, this);

                if (this.attachment) {
                    existCVs.forEach(function (item, i) {
                        if ((item.name + '.' + item.ext) === delName) {
                            this.deleteFromServer(item.url);

                            existCVs.splice(i, 1);
                            this.attachment.set({cv: existCVs});
                        }
                    }, this);

                    existPhotos.forEach(function (item, i) {
                        if ((item.name + '.' + item.ext) === delName) {
                            this.deleteFromServer(item.url);

                            existPhotos.splice(i, 1);
                            this.attachment.set({photo: existPhotos});
                        }
                    }, this);
                } 
            }, this);
        },

        deleteFromServer: function (_url) {
            $.ajax({
                url: _url,
                type: "DELETE",
                success: function (data) {
                    console.log(data);
                },
                error: function (xhr, status, error) {
                    console.log(error + status);
                }
            });
        }
    });
})(CS.Students, app);