'use strict';
(function (This, app) {
    This.WeekEditorView = Backbone.View.extend({
        tagName: 'div',
        className: 'scheduleEditorWeek-view',
        template: templates.weekViewTpl,

        subscribes: {
            'Week: selected': 'render',
            'WeekView: current state' : 'getCurrentInputsState',
            'Event: checked' : 'addEventExist',
            'NextWeek: selected': 'renderNextWeek',
            'PrevWeek: selected': 'renderPreviousWeek',
            'WeekView: weekView rendered': 'pubNumbersWeeks'
       },

        events: {
            'click': 'addEvent'
        },

        initialize: function () {
            var groupNameList,
                day;

            app.mediator.multiSubscribe(this.subscribes, this);

            this.currentWeekScheduleSet = [];            

            this.currentWeek = this.getCurrentWeek();

            this.week = {};

            if (this.collection[0].get('groupName') === '') {
                this.collection[0].set('groupName', this.collection[0].get('groupKey'));
            }

            this.weekList = app.filter.split('groupSchedule', {weekList: this.collection});

            this.activity = {};
            this.hourActivity = {'monday': {}, 'tuesday': {}, 'wednesday': {}, 'thursday': {}, 'friday': {}};
            this.nodeRouter = {'monday': 1, 'tuesday': 2, 'wednesday': 3, 'thursday': 4, 'friday': 5};
            this.timeRouter = {'09:00': 'nine',
                               '09:30': 'nine-half',
                               '10:00': 'ten',
                               '10:30': 'ten-half',
                               '11:00': 'eleven',
                               '11:30': 'eleven-half',
                               '12:00': 'twelve',
                               '12:30': 'twelve-half',
                               '13:00': 'thirteen',
                               '13:30': 'thirteen-half',
                               '14:00': 'fourteen',
                               '14:30': 'fourteen-half',
                               '15:00': 'fifteen',
                               '15:30': 'fifteen-half',
                               '16:00': 'sixteen',
                               '16:30': 'sixteen-half',
                               '17:00': 'seventeen',
                               '17:30': 'seventeen-half',
                               '18:00': 'eighteen',
                               '18:30': 'eighteen-half',
                               '19:00': 'nineteen',
                               '19:30': 'nineteen-half',
                               '20:00': 'twenty'
                              };
        },

        getCurrentInputsState: function (currentState) {
            this.activity = currentState;
        },

        callData: function () {
            app.mediator.publish('ScheduleEditor: get state');
        },

        preRender: function () {
            var currentWeekSchedule;

            currentWeekSchedule = this.collection[0].get('weeks')[this.currentWeek];

            if (currentWeekSchedule) {

                for (var day in currentWeekSchedule) {
                    this.week[day] = [];

                    if (currentWeekSchedule[day]) {
                        _.forEach(currentWeekSchedule[day], function (activity) {
                            this.week[day].push(activity);
                        }, this);
                    }
                }

                for (var day in this.week) {
                    this.pushToActivityStore(day);
                }
            }
        },

        render: function () {
			var currentWeekInfo = this.weekList[0][this.currentWeek];
			            
			this.$el.append(this.template(this.model));
            this.renderDates(this.currentWeek);
                        
            if (currentWeekInfo) {
                this.renderIteration();
            }
			
			app.mediator.publish('Week: ready for render', currentWeekInfo);
            return this;
        },

        addEventExist: function (element, model) {
            var isEqual = false,
                data = element.dataset,
                day = data.day,
                time = data.time,
                activityView, $a,
                activity, calibre;

            this.callData();

            activity = this.initActivity(time);

            isEqual =  this.isEqualData(day, time, activity);

            if (!isEqual) {
                activityView = new This.ActivityEditorView({model: activity, style:  this.week[day].length});

                this.setHourActivity(day, time, activityView);
                calibre = this.getHourActivity(day, time);

                if (this.week[day]) {
                    this.week[day].push(activity);
                } else {
                    this.week[day] = [];
                    this.week[day].push(activity);
                }

                $a = activityView.render(activity.duration).$el;

                this.setActivityCss(day, time);

                $(element).append($a);
                $(element.firstChild).css({
                        'width': (100/calibre) + '%'
                });

                if ($(element.firstChild).width() < 50) {
                    $(element.firstChild).find('p').remove();
                }

                if ($a.width() < 50) {
                    $a.find('p').remove();
                }
            }
        },

        isEqualData: function (day, time, newActivity) {
            var activityList = this.week[day],
                timeActivityList = [],
                isEqual = false;

            activityList.forEach(function (activity) {
                if (activity.startTime === time) {
                    timeActivityList.push(activity);
                }
            });

            timeActivityList.forEach(function (activity) {
                if (activity.room === newActivity.room || activity.teacher === newActivity.teacher) {
                    isEqual = true;
                }
            });

            return isEqual;
        },

        setActivityCss: function (day, time) {
            var activityList = this.hourActivity[day][time],
                calibre = activityList.length;

            activityList.forEach(function (activityView) {
                activityView.$el.css({
                    'width': (100/calibre) + '%',
                    'height': (activityView.duration*200 + Number(activityView.duration)*1.6945)+'%',
                    'border-left': '1px dashed'
                });
            });
        },

        addEvent: function (evt) {
            var target = evt.target,
                data = target.dataset,
                day = data.day,
                time = data.time,
                isEqualData = false,
                isCrossTime = false,
                lastTime = '21:00',
                $a, activityView,
                event, activity,
                calibre, nextCellTime;

            evt.preventDefault();
            evt.stopPropagation();

            this.callData();

            activity = this.initActivity(time);

			if (activity.title === 'Not selected') {
				return;
			}
			
            isCrossTime = this.isCrossTime(day, time);

            for(event in this.week[day]) {
               if ((this.week[day][event].teacher === activity.teacher || this.week[day][event].room === activity.room) && this.week[day][event].startTime=== activity.startTime) {
                    isEqualData = true;
                }
            }

            if (time === lastTime && Number(activity.duration) >= 1) {
                isEqualData = true;
            }

            if (this.isCrossTime(day, time)) {
                isEqualData = true;
            }

            nextCellTime = this.getNextCellTime(activity.startTime, activity.duration);

            if (!isEqualData && !this.hourActivity[day][nextCellTime]) {
                this.setHourActivity(day, activity.startTime);

                calibre = this.getHourActivity(day, time);


                 if (this.week[day]) {
                    this.week[day].push(activity);
                } else {
                    this.week[day] = [];
                    this.week[day].push(activity);
                }

                activityView = new This.ActivityEditorView({model: activity, style:  this.week[day].length});
                $a = activityView.render(activity.duration).$el;

                $a.css({
                    'width': (100/calibre)+'%',
                    'height': (activity.duration*200 + Number(activity.duration)*1.6945)+'%',
                    'border-left': '1px dashed'
                });

                $(target).append($a);
				app.mediator.publish('ScheduleEditor: activity added', activity);
            }

        },

        isCrossTime: function (day, time) {
            var dayActivityList = this.week[day],
                isCross = false,
                startTime = 0,
                activityTime = 0,
                newTime = 0;

            if (dayActivityList) {
                dayActivityList.forEach((function (activity) {
                    startTime = parseInt(activity.startTime.split(':').join(''));
                    activityTime = parseInt(this.getNextCellTime(activity.startTime, activity.duration).split(':').join(''));
                    newTime = parseInt(time.split(':').join(''));

                    if (newTime <= activityTime && newTime >= startTime) {
                        isCross = true;
                    }
                }).bind(this));
            }

            return isCross;
        },

        renderIteration: function () {
            var calculatedMarginLeft,
                activityView,
                activity,
                calibre,
                $div,
                $day,
                day,
                $a;

            this.preRender();

            for (day in this.week) {

                this.week[day].forEach(function (activity, i) {
                    if (activity.duration) {

                        activityView = new This.ActivityEditorView({model: activity, style:  this.week[day].length});

                        this.setHourActivity(day, activity.startTime, activityView);

                        $div =  this.$el.find('.'+this.timeRouter[activity.startTime]);
                        $day = $($div[0].childNodes[this.nodeRouter[day]]);


                        $a = activityView.render(activity.duration).$el;

                        this.setActivityCss(day, activity.startTime);

                        calculatedMarginLeft = '0%';

                        if (day !== 'thursday'){
                            $a.css({
                                 'margin-left': calculatedMarginLeft
                            });
                        }

    					$day.append($a);

    				if ($a.width() < 50) {
    						$a.find('p').remove();
    				}

    				$a.mouseover(function () {
    					var hints = [];

                        if ($a.width() < 50) {
    						$a.attr('name', i);
    						hints.push({
    							name: $a.attr('name'),
    							text: activity.title + ' ' + activity.teacher + ' ' + activity.room
    						});
    					}

                        if (hints !== []) {

    						app.mediator.publish('Message', {
    							type: 'hints',
    							$el: this.$el,
    							hints: hints,
    							coordinates: $a.offset()
    						});
    					}
    				}.bind(this));

    				$a.mouseleave(function () {
    					this.$el.find('.hint').remove();
    				}.bind(this));
                };

                }.bind(this));
            };
        },

        initActivity: function (time) {
            var activity = {
                    title: '',
                    teacher: '',
                    startTime: '',
                    duration: '1',
                    room: ''
                };

            activity.startTime = time;
            activity.teacher = this.activity['resourceteacher'];
            activity.event = this.activity['event'];
            activity.title = this.activity['eventTitle'];
            activity.room = this.activity['room'];

            return activity;
        },

        setHourActivity: function (day, time, activity) {
            if (this.hourActivity[day][time]) {
                this.hourActivity[day][time].push(activity);
            } else {
                this.hourActivity[day][time] = [];
                this.hourActivity[day][time].push(activity);
            }

            return  this.hourActivity[day][time];
        },

        getHourActivity: function (day, time) {
            var calibre;

            if (this.hourActivity[day][time]) {

                calibre = this.hourActivity[day][time].length;
            }

            return calibre;
        },

        getNextCellTime: function (startTime, _duration) {
            var duration = (Number.parseInt(_duration) * 2) - 1,
                nextCellTime = startTime,
                i;

          for (i = 0; i < duration; i++) {
            nextCellTime = this.oneDurationIteration(nextCellTime);
          }

          return nextCellTime;
        },

        oneDurationIteration: function (startTime) {
            var nextCellTime = startTime.split(':');

            if (nextCellTime[1] === '30') {
                nextCellTime[0] = Number.parseInt(nextCellTime[0]) + 1;
                nextCellTime[1] = '00';

                if (nextCellTime[0] < 10) {
                    nextCellTime[0] = '0' + nextCellTime[0];
                }

            } else if (nextCellTime[1] === '00') {
                nextCellTime[1] = '30';
            }

            return nextCellTime[0] + ':' + nextCellTime[1];
        },

        pushToActivityStore: function (day) {
            var dayActivityStore;

            dayActivityStore = {
                    '09:00': [],
                    '09:30': [],
                    '10:00': [],
                    '10:30': [],
                    '11:00': [],
                    '11:30': [],
                    '12:00': [],
                    '12:30': [],
                    '13:00': [],
                    '13:30': [],
                    '14:00': [],
                    '14:30': [],
                    '15:00': [],
                    '15:30': [],
                    '16:00': [],
                    '16:30': [],
                    '17:00': [],
                    '17:30': [],
                    '18:00': [],
                    '18:30': [],
                    '19:00': [],
                    '19:30': [],
                    '20:00': []
            };

            this.week[day].forEach(function (activity, i) {
                for (var time in dayActivityStore) {
                    if (this.isLater(time, activity.startTime) && (this.isLater2(this.defineDuration(activity.startTime, activity.duration), time))) {
                        dayActivityStore[time].push(activity);
                    }
                }
            }.bind(this));
        },

        isLater: function (stringA, stringB) {

            return this.convertToNumber(stringA) >= this.convertToNumber(stringB);
        },

        isLater2: function (stringA, stringB) {

            return this.convertToNumber(stringA) > this.convertToNumber(stringB);
        },

        defineDuration: function (str, duration) {
            var convertedToNumberTime = this.convertToNumber(str);

            if (str.substr(3,1) === '3') {
                if (String(duration).length > 1) {
                    convertedToNumberTime+= 70 + String(duration).substr(0,1)* 100;
                } else {
                    convertedToNumberTime+= duration * 100;
                }
            } else {
                if (String(duration).length > 1) {
                    convertedToNumberTime+= 30 + String(duration).substr(0,1)* 100;
                } else {
                    convertedToNumberTime+= duration * 100;
                }
            }
            return this.convertToTimeString(convertedToNumberTime);
        },

        convertToTimeString: function (number) {
            var string = String(number),
                result = '';

            if (string.length === 4) {
                result = string.substr(0,2) + ':' + string.substr(2,2);
            } else {
                result = '0'+ string.substr(0,1) + ':' + string.substr(1,2);
            }

            return result;
        },

        convertToNumber: function (timeString) {
            var string = timeString.replace(':', '');

            return Number(string);
        },

        renderNextWeek: function () {
            var currentWeek = this.toMomentFormat(this.currentWeek);

            this.clearHourActivity();
            this.currentWeek = moment(currentWeek).add(7, 'days').format('MMDDYYYY');
            this.render();
            this.pubNumbersWeeks();
        },

        renderPreviousWeek: function () {
            var currentWeek = this.toMomentFormat(this.currentWeek);

            this.clearHourActivity();
            this.currentWeek = moment(currentWeek).subtract(7, 'days').format('MMDDYYYY');
            this.render();
            this.pubNumbersWeeks();
        },

        getCurrentWeek: function () {
            var daysFromMonday,
                currentDate,
                dayOfWeek,
                monday;

            currentDate = new Date(),
            dayOfWeek = currentDate.getDay();
            daysFromMonday = dayOfWeek - 1;
            monday = moment().subtract(daysFromMonday,'days').format('MMDDYYYY');

            return monday;
        },

        renderDates: function (monday) {
            var weekDay,
                currentMonth,
                headerDays,
                currentDay;

            headerDays = this.$el.find('.number-week-wrapper');

            headerDays.each(function (i, elem) {

                weekDay = this.toMomentFormat(monday);
                weekDay = moment(weekDay).add(i, 'days').format('MMDDYYYY');

                currentMonth = weekDay.slice(0,2);
                $(elem).find('.month').text(currentMonth);

                currentDay = weekDay.slice(2,4);
                $(elem).find('.day').text(currentDay);

            }.bind(this));
        },

        toMomentFormat: function (date) {
            var formattedDate,
                monthAndDay,
                year;

            year = date.slice(4);
            monthAndDay = date.slice(0,4);

            return formattedDate = year + monthAndDay;
        },
        getTotalWeeks: function () {
            var finishDate,
                startDate,
                weekNumber;
 
            finishDate = this.collection[0].get('keyDates').finish;
            startDate = this.collection[0].get('keyDates').start;

            weekNumber = this.getDaysBetweenDates(startDate, finishDate);
            weekNumber = Math.ceil(weekNumber/7);

            return weekNumber;
        },

        pubNumbersWeeks: function () {
            var weekNumbers = {};
                              
            weekNumbers.totalWeekNumber = this.getTotalWeeks();
            weekNumbers.currentWeekNumber = this.getWeekNumber(this.currentWeek);
           
            app.mediator.publish('ScheduleEditor: total weeks', weekNumbers);
        },    

        getWeekNumber: function (monday) {
            var currentDate,
                weekNumber,
                totalWeeks,
                startDate;
            
            currentDate = monday.slice(0,2) + '/' + monday.slice(2,4) + '/' + monday.slice(4);
            startDate = this.collection[0].get('keyDates').start;

            weekNumber = this.getDaysBetweenDates(startDate, currentDate);
            weekNumber = Math.ceil(weekNumber/7);
            totalWeeks = this.getTotalWeeks();

            if (weekNumber > totalWeeks) {
                weekNumber = totalWeeks;
            }
             
            if (weekNumber < 0) {
                weekNumber = 0;
            } 

            return weekNumber;
        },


        getDaysBetweenDates: function (prevDate, nextDate) {
            var finishDate,
                startDay,
                diffTime,
                diffDays;

            finishDate = new Date(nextDate.slice(6), Number(nextDate.slice(0,2)) - 1, nextDate.slice(3,5));
            startDay = new Date(prevDate.slice(6), Number(prevDate.slice(0,2)) - 1, prevDate.slice(3,5));
            
            diffTime = finishDate.getTime() - startDay.getTime();
            diffDays = Math.ceil(diffTime / (1000 * 3600 * 24));

            return diffDays;
        },

        clearHourActivity: function () {
            var weeks = this.collection[0].get('weeks'),
                day, hour;

            weeks[this.currentWeek] = {};

            for (day in this.hourActivity) {
                for (hour in this.hourActivity[day]) {
                    if (this.hourActivity[day][hour]) {
                        if (this.hourActivity[day][hour].length) {
                            this.hourActivity[day][hour].forEach(function (view) {
                                view.remove();
                            });     
                        }
                    }
                }   
                this.hourActivity[day] = {};
            }

            for (var key in this.week) {
              weeks[this.currentWeek][key] = this.week[key];
              this.week[key] = {};
            }

            this.collection[0].save();
        }
    });
})(CS.ScheduleEditor, app);
